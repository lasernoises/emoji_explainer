import sys
import json

with open('assets/emoji.json') as emoji_data:
    d = json.load(emoji_data)

    if len(sys.argv) >= 2 and sys.argv[1] in d["emojis"]:
        value = d["emojis"][sys.argv[1]]
        print(value)
    else:
        print("unknown emoji")
